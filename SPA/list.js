function List(data) {
  const table = document.getElementById("table");

  function render() {
    while (table.children.length > 0) {
      table.removeChild(table.lastChild);
    }

    data.forEach((item, id) => {
      createElement(item.name, item.phone, id);
    });
  }

  this.addItem = function(name, phone) {
    data.push({
      id: data.length + 1,
      name,
      phone
    });
    render();
  };

  this.editItem = function(newName, newPhone, id) {
    data[id].name = newName;
    data[id].phone = newPhone;
    render();
  };

  this.delItem = function(id) {
    data.splice(id, 1);
    render();
  };

  this.renderList = function() {
    render();
  };
}
