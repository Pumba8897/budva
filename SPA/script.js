console.log(data);

const list = new List(data);
list.renderList();

// console.log(data);
// // function Item(name, phone) {
// //   newItem = document.createElement("div");
// //   newItem.classList.add("item");

// //   fieldName = document.createElement("input");
// //   fieldPhone = document.createElement("input");
// //   editButton = document.createElement("button");
// //   deleteButton = document.createElement("button");
// //   fieldName.disabled = true;
// //   fieldPhone.disabled = true;
// //   fieldName.value = name;
// //   fieldPhone.value = phone;
// //   editButton.textContent = "редактировать";
// //   deleteButton.textContent = "удалить";

// //   newItem.append(fieldName);
// //   newItem.append(fieldPhone);
// //   newItem.append(editButton);
// //   newItem.append(deleteButton);
// //   return newItem;
// // }

function validation(name, phone) {
  let isValid = false;
  if (name && phone) {
    const regExp = /^\+?[0-9\-]+$/g;
    isValid = regExp.test(phone);
    if (!isValid) alert("Проверьте номер телефона");
  } else {
    alert("Заполние все поля");
  }
  return isValid;
}

function delItem(elem) {
  const id = elem.parentNode.dataset.id;
  list.delItem(id);
}

function editItem(elem) {
  const parent = elem.parentNode;
  parent.querySelector(".nameInput").disabled = false;
  parent.querySelector(".phoneInput").disabled = false;
  elem.insertAdjacentHTML(
    "beforeend",
    `<span class="saveBtn" onclick="saveItem(this)">💾</span>`
  );
}

function saveItem(elem) {
  const parent = elem.parentNode.parentNode;
  const name = parent.querySelector(".nameInput").value;
  const phone = parent.querySelector(".phoneInput").value;
  if (validation(name, phone)) {
    list.editItem(name, phone, parent.dataset.id);
    elem.remove();
  }
}
function addItem() {
  const name = document.getElementById("name").value;
  const phone = document.getElementById("phone").value;

  const phoneValid = validation(name, phone);
  if (phoneValid) {
    list.addItem(name, phone);
  }
}

document.getElementById("addButton").addEventListener("click", addItem);
