const bigCanvas = document.getElementById("bigCanvas");
const ctxBig = bigCanvas.getContext("2d");

function Star(color, x, y) {
  this.fillColor = color;
  this.x = x;
  this.y = y;
  ctxBig.beginPath();
  ctxBig.fillStyle = this.fillColor;
  ctxBig.moveTo(this.x, this.y);
  ctxBig.lineTo(this.x + 30, this.y + 60);
  ctxBig.lineTo(this.x + 90, this.y + 60);
  ctxBig.lineTo(this.x + 40, this.y + 110);
  ctxBig.lineTo(this.x + 70, this.y + 180);
  ctxBig.lineTo(this.x, this.y + 140);
  ctxBig.lineTo(this.x - 70, this.y + 180);
  ctxBig.lineTo(this.x - 40, this.y + 110);
  ctxBig.lineTo(this.x - 90, this.y + 60);
  ctxBig.lineTo(this.x - 30, this.y + 60);
  ctxBig.lineTo(this.x, this.y);
  ctxBig.fill();
}

const redStar = new Star("red", 150, 10);
const blueStar = new Star("blue", 400, 10);
const greenStar = new Star("green", 150, 200);
const yellowStar = new Star("yellow", 400, 200);
const blackStar = new Star("black", 150, 400);

const smallCanvas = document.getElementById("smallCanvas");
const ctxSmall = smallCanvas.getContext("2d");

ctxSmall.rect(0, 0, 600, 50);
ctxSmall.stroke();

document.getElementById("bigCanvas").addEventListener("click", e => {
  const pixel = ctxBig.getImageData(e.clientX, e.clientY, 1, 1);
  const colorArr = pixel.data;
  const color = `rgba(${colorArr[0]},${colorArr[1]},${colorArr[2]},${colorArr[3]})`;

  ctxSmall.fillStyle = colorArr[3] !== 0 ? color : "rgba(255, 255, 255, 255)";
  ctxSmall.fill();
  ctxSmall.stroke();
});
